using YAML
using Dates


function create_insert(user, factors)
    datetimenow = Dates.value(Dates.now())
    population_table = string("$user", "_population_weights_$datetimenow")
    sql_create = """
                 CREATE TABLE $population_table (
                     feature TEXT PRIMARY KEY,
                     weight DOUBLE
                 );
                 """
    sql_insert = "INSERT INTO $population_table VALUES\n"
    factors_size = length(factors)
    i = 0
    for (feature, weight) in factors
        sql_insert = string(sql_insert, "    ($feature, $weight)")
        i += 1
        if i == factors_size
            sql_insert = string(sql_insert, "\n;")
        else
            sql_insert = string(sql_insert, ", \n")
        end
    end
    return (table=population_table, create=sql_create, insert=sql_insert)
end

data = YAML.load(open("data/config.yml"))
user = data["user"]
id = data["identity"]
factors = data["factors"]
sql = create_insert(user, factors)
println(sql.table)
println(sql.create)
println(sql.insert)

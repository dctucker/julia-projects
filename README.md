# julia-projects


Epoch-1: max % balance = 0.6788306315761743
Epoch-2: max % balance = 0.5202107014336392
Epoch-3: max % balance = 0.3547306504740917
Epoch-4: max % balance = 0.21707414922240575
Epoch-5: max % balance = 0.12248029631260837
Epoch-6: max % balance = 0.06563403160528214
Epoch-7: max % balance = 0.034162429363937985
Epoch-8: max % balance = 0.017534026843659345
Epoch-9: max % balance = 0.012230081531625747
Epoch-10: max % balance = 0.008554137173118703


http://faculty.nps.edu/rdfricke/docs/RakingArticleV2.2.pdf
http://www.stat.columbia.edu/~cook/writeup.pdf
http://www.sverigeisiffror.scb.se/contentassets/ca21efb41fee47d293bbee5bf7be7fb3/weighting-methods.pdf
http://www.nyu.edu/classes/jackson/design.of.social.research/Readings/Johnson%20-%20Introduction%20to%20survey%20weights%20%28PRI%20version%29.pdf

using LinearAlgebra
using Random

# https://towardsdatascience.com/introduction-to-optimization-with-genetic-algorithm-2f5001d9964b
function fitness(old_state, new_state)
    old_value = dot(old_state, costs)
    old_dist_err = (sum((((ratio .* costs) ./ (old_state .* costs)) / value).^4))^(1/4)
    old_err = (value - old_value) * (1 - old_dist_err)

    new_value = dot(new_state, costs)
    new_dist_err = (sum((((ratio .* costs) ./ (new_state .* costs)) / value).^4))^(1/4)
    new_err = (value - new_value) * (1 - new_dist_err)

    if new_value > value
        return old_state
    elseif minimum(new_state) <= 0
        return old_state
    elseif new_err > old_err
        return old_state
    else
        return new_state
    end
end


function mutate(epochs=100, state=[1, 1, 1])
    rng = MersenneTwister()
    chg = Random.Sampler(rng, -1:1)

    for i=1:epochs
        r = rand()
        if r <= cumsum_ratio[1]
            idx = 1
        elseif r <= cumsum_ratio[2]
            idx = 2
        else
            idx = 3
        end

        new_state = deepcopy(state)
        new_state[idx] += rand(rng, chg)
        state = fitness(state, new_state)
    end
    return state
end


value = 10000.00
bnd=84.08
vxus=57.43
vti=166.44
costs = [bnd, vxus, vti]
ratio = [0.5, 0.25, 0.25]
ratio = (ratio ./ costs)/sum(ratio ./ costs)
performance = [3.0, 5.87, 11.21]

state = mutate(value/10)
value = dot(state, costs)
pct = (state .* costs) / value
stocks = (state .* costs)
println("end percent: $pct")
println("end stocks: $stocks")
println("end state: $state")
println("end value: $value")
println()
println()

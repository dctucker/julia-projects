using SQLite
using Distributions: Binomial
using Random
using Statistics
using DataFrames


gender_1 = 0.5
gender_2 = 0.5
race_1 = 0.3
race_2 = 0.7
region_1 = 0.5
region_2 = 0.5

function create_database()
    s = 10000
    db = SQLite.DB("data/SampleDB.db")
    customer = [i for i in 1:s]
    sample_gender_1 = rand(Binomial(1, 0.4), s)
    sample_gender_2 = rand(Binomial(1, 0.6), s)
    sample_race_1 = rand(Binomial(1, 0.7), s)
    sample_race_2 = rand(Binomial(1, 0.3), s)
    sample_region_1 = rand(Binomial(1, 0.1), s)
    sample_region_2 = rand(Binomial(1, 0.9), s)
    weight = ones(s)

    df = DataFrame(customer=customer,
                   gender_1=sample_gender_1, gender_2=sample_gender_2,
                   race_1=sample_race_1 , race_2=sample_race_2,
                   region_1=sample_region_1, region_2=sample_region_2,
                   weight=weight)

    SQLite.load!(df, db, "sample")

    sql = """
          CREATE TABLE population (
            factor VARCHAR(8) PRIMARY KEY,
            weight DOUBLE
          );
          """
    SQLite.execute!(db, sql)

    sql = """
          INSERT INTO population VALUES
            ("gender_1", $gender_1),
            ("gender_2", $gender_2),
            ("race_1", $race_1),
            ("race_2", $race_2),
            ("region_1", $region_1),
            ("region_2", $region_2);
          """
    SQLite.execute!(db, sql)
end

# http://www.nyu.edu/classes/jackson/design.of.social.research/Readings/Johnson%20-%20Introduction%20to%20survey%20weights%20%28PRI%20version%29.pdf
function rake(db, thing)
    sql = "DROP TABLE IF EXISTS reweight;"
    SQLite.execute!(db, sql)

    sql = """
          CREATE TABLE reweight AS
            WITH r as (
                SELECT w.weight / (SUM(CASE WHEN s.$thing = 1 THEN s.weight END) / SUM(s.weight)) as gW
                    FROM sample as s
                        CROSS JOIN (SELECT weight FROM population WHERE factor = '$thing') as w
            )
            SELECT customer, weight * gW as weight
                FROM sample as s
                    CROSS JOIN r as rw
                WHERE s.$thing = 1;
          """
    SQLite.execute!(db, sql)

    sql = """
          UPDATE sample
            SET weight = (SELECT weight FROM reweight WHERE sample.customer = reweight.customer)
            WHERE EXISTS (SELECT * FROM reweight WHERE sample.customer = reweight.customer);
          """
    SQLite.execute!(db, sql)

    sql = "DROP TABLE reweight;"
    SQLite.Query(db, sql)
end

create_database()
db = SQLite.DB("data/SampleDB.db")
columns = [(gender_1, "gender_1"), (gender_2, "gender_2"),
           (race_1, "race_1"), (race_2, "race_2"),
           (region_1, "region_1"), (region_2, "region_2")]


i = 0
m = 1

while m > 0.01
    err = []
    global i += 1
    for (sy, col) in columns
        rake(db, col)

        sql = """
              SELECT (SUM(CASE WHEN s.$col = 1 then s.weight END) / SUM(s.weight)) as gW
                FROM sample as s
              """
        df = DataFrame(SQLite.Query(db, sql)).gW[1]
        prop = abs(1 - (df / sy))
        append!(err, prop)
    end
    global m = maximum(err)
    println("Epoch-$i: max % balance = $m")
end

using Distributions: Binomial
using Random
using Statistics
using DataFrames

s = 1000000
gender_1 = 0.5
gender_2 = 0.5
race_1 = 0.3
race_2 = 0.7
region_1 = 0.5
region_2 = 0.5

sample_gender_1 = rand(Binomial(1, 0.4), s)
sample_gender_2 = rand(Binomial(1, 0.6), s)
sample_race_1 = rand(Binomial(1, 0.7), s)
sample_race_2 = rand(Binomial(1, 0.3), s)
sample_region_1 = rand(Binomial(1, 0.1), s)
sample_region_2 = rand(Binomial(1, 0.9), s)
weight = ones(s)

# http://www.nyu.edu/classes/jackson/design.of.social.research/Readings/Johnson%20-%20Introduction%20to%20survey%20weights%20%28PRI%20version%29.pdf
function rake(sample, filter, population)
   f = sample[:, Symbol(filter)] # get filtered records
   W = sum(sample[f .== 1, :weight]) / sum(sample.weight) # sample prop portion
   gW = population / W # create weight
   sample[f .== 1, :weight] .= sample[f .== 1, :weight] * gW # apply weight
   return sample
end

sample = DataFrame(gender_1=sample_gender_1, gender_2=sample_gender_2,
                   race_1=sample_race_1 , race_2=sample_race_2,
                   region_1=sample_region_1, region_2=sample_region_2,
                   weight=weight)

g = [(gender_1, "gender_1"), (gender_2, "gender_2"),
     (race_1, "race_1"), (race_2, "race_2"),
     (region_1, "region_1"), (region_2, "region_2")]
for i = 1:10
   for (var, fac) in g
      rake(sample, fac, var)
   end
end

for (var, fac) in g
   println("factor: $fac")
   println("population: $var")
   f = sample[:, Symbol(fac)]
   w = sum(sample[f .== 1, :weight]) / sum(sample.weight)
   uw = mean(f)
   max = maximum(sample[f .== 1, :weight])
   avg = mean(sample[f .== 1, :weight])
   println("un-weighted: $uw")
   println("weighted: $w")
   println("max weight: $max")
   println("mean weight: $avg")
   println()
end
